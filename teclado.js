function playSound(event) {
    let key = event.keyCode;
    let player1 = document.querySelector("audio").getAttribute("data-key");
    let audio1 = document.querySelector(`audio[data-key= "${key}"]`);

    audio1.currentTime = 0;
    audio1.play();

}
window.addEventListener("keydown", playSound);

/*function playSound (event){
    document.querySelector("div").getAttribute(data-key==83);

}
playSound();*/